#!/usr/bin/env bash
WORKDIR="$( cd "$(dirname "$0")" ; pwd -P )"
cd ${WORKDIR}/tests
PYTHONPATH=.. python -m unittest workgroups_test
