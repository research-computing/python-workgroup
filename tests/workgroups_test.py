#!/usr/bin/python
#
# Stanford Research Computing Center
#
# Test of workgroups.py
# Usage: nosetests -sv workgroups_test.py
#
# Aug 12, 2015 sthiell: Created


from random import randint
import unittest

from srcc.utils.workgroup import SUWorkgroupManager, SUWorkgroupAPIError
from srcc.utils.workgroup import FILTER_NONE, FILTER_STAFF
from srcc.utils.workgroup import VISIBILITY_STANFORD, VISIBILITY_PRIVATE

###############################################################################
#                                    TESTS                                    #
###############################################################################
# For these tests to work, you need a valid CERT/CERT_KEY added as an
# administrator of the stem defined below.
TEST_CERT = 'certs/xstream.stanford.edu.cert'
TEST_CERT_KEY = 'certs/xstream.key'

# Tests will create and manipulate the TEST_WRKGRP workgroup in TEST_STEM
TEST_STEM = 'research-computing'
TEST_WRKGRP = 'test-api4'

# We also need some SunetIDs and a parent workgroup that are used by the tests
TEST_MEMBER1 = 'sthiell'
TEST_MEMBER2 = 'kilian'
TEST_ADMIN_MEMBER = 'pwtest'
TEST_PARENT_WRKGRP = 'research-computing:test-api-root'


class SUWorkgroupTest(unittest.TestCase):

    def setUp(self):
        if not hasattr(self, 'manager'):
            self.test_wrkgrp_name = "%s:%s" % (TEST_STEM, TEST_WRKGRP)
            self.manager = SUWorkgroupManager(cert=TEST_CERT, cert_key=TEST_CERT_KEY)

    # Disabled at it's not possible to re-create a workgroup AFAIK
    # Please see INC000003451746
    #
    #def test_001_cleanup(self):
    #    """test initial workgroup cleanup"""
    #    try:
    #        self.manager.delete_workgroup(self.test_wrkgrp_name)
    #    except SUWorkgroupAPIError, e:
    #        print e
    #        self.assertEqual(e.status_code, 404)

    #
    # XXX Use create test once to create test workgroups!
    #
    #def test_002_create(self):
    #    """test create workgroup"""
    #    try:
    #        self.manager.create_workgroup(self.test_wrkgrp_name,
    #                                      "Test API workgroup")
    #    except SUWorkgroupAPIError, e:
    #        print e

    def test_001_workgroup_exists(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertEqual(workgroup.name, self.test_wrkgrp_name)
        self.assertTrue(len(workgroup.description) > 0)
        self.assertEqual(workgroup.filter, FILTER_NONE)
        self.assertEqual(workgroup.visibility, VISIBILITY_STANFORD)
        self.assertTrue(workgroup.reusable)
        self.assertTrue(workgroup.privgroup)

    def test_002_update_property_description(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        new_desc = "Test workgroup for API Python bindings (%d)" % randint(0, 1e10)
        workgroup.description = new_desc
        self.assertEqual(workgroup.description, new_desc)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertEqual(workgroup.description, new_desc)

    def test_003_update_property_filter(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        workgroup.filter = FILTER_STAFF
        self.assertEqual(workgroup.filter, FILTER_STAFF)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertEqual(workgroup.filter, FILTER_STAFF)

        workgroup.filter = FILTER_NONE
        self.assertEqual(workgroup.filter, FILTER_NONE)

    def test_004_update_property_reusable(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        new_reusable = not workgroup.reusable
        workgroup.reusable = new_reusable
        self.assertEqual(workgroup.reusable, new_reusable)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertEqual(workgroup.reusable, new_reusable)

        if not new_reusable:
            workgroup.reusable = True
            self.assertTrue(workgroup.reusable)

    def test_005_update_property_visibility(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        if workgroup.visibility == VISIBILITY_PRIVATE:
            new_visibility = VISIBILITY_STANFORD
        else:
            new_visibility = VISIBILITY_PRIVATE
        workgroup.visibility = new_visibility
        self.assertEqual(workgroup.visibility, new_visibility)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertEqual(workgroup.visibility, new_visibility)

        if new_visibility == VISIBILITY_PRIVATE:
            workgroup.visibility = VISIBILITY_STANFORD
            self.assertEqual(workgroup.visibility, VISIBILITY_STANFORD)

    def test_006_update_property_privgroup(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        new_privgroup = not workgroup.privgroup
        workgroup.privgroup = new_privgroup
        self.assertEqual(workgroup.privgroup, new_privgroup)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertEqual(workgroup.privgroup, new_privgroup)

        if not new_privgroup:
            workgroup.privgroup = True
            self.assertTrue(workgroup.privgroup)

    def test_007_add_member(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        workgroup.add_member(TEST_MEMBER1)
        self.assertTrue(TEST_MEMBER1 in workgroup.members)
        workgroup.add_member(TEST_MEMBER2)
        self.assertTrue(TEST_MEMBER2 in workgroup.members)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertTrue(TEST_MEMBER1 in workgroup.members)
        self.assertTrue(TEST_MEMBER2 in workgroup.members)

    def test_008_remove_member(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        workgroup.remove_member(TEST_MEMBER1)
        self.assertFalse(TEST_MEMBER1 in workgroup.members)
        workgroup.remove_member(TEST_MEMBER2)
        self.assertFalse(TEST_MEMBER2 in workgroup.members)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertFalse(TEST_MEMBER1 in workgroup.members)
        self.assertFalse(TEST_MEMBER2 in workgroup.members)

    def test_009_add_administrator(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        workgroup.add_administrator(TEST_ADMIN_MEMBER)
        self.assertTrue(TEST_ADMIN_MEMBER in workgroup.administrators)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertTrue(TEST_ADMIN_MEMBER in workgroup.administrators)

    def test_010_remove_administrator(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        workgroup.remove_administrator(TEST_ADMIN_MEMBER)
        self.assertFalse(TEST_ADMIN_MEMBER in workgroup.administrators)

        self.manager.clear()
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        self.assertFalse(TEST_ADMIN_MEMBER in workgroup.administrators)


    def test_011_add_member_workgroup(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        parent.add_workgroup(workgroup)
        self.assertTrue(self.test_wrkgrp_name in parent.workgroups)

        self.manager.clear()
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        self.assertTrue(self.test_wrkgrp_name in parent.workgroups)

    def test_012_remove_member_workgroup(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        parent.remove_workgroup(workgroup)
        self.assertFalse(self.test_wrkgrp_name in parent.workgroups)

        self.manager.clear()
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        self.assertFalse(self.test_wrkgrp_name in parent.workgroups)

    def test_013_add_admin_workgroup(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        parent.add_admin_workgroup(workgroup)
        self.assertTrue(self.test_wrkgrp_name in parent.admin_workgroups)

        self.manager.clear()
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        self.assertTrue(self.test_wrkgrp_name in parent.admin_workgroups)

    def test_014_remove_admin_workgroup(self):
        workgroup = self.manager.workgroup(self.test_wrkgrp_name)
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        parent.remove_admin_workgroup(workgroup)
        self.assertFalse(self.test_wrkgrp_name in parent.admin_workgroups)

        self.manager.clear()
        parent = self.manager.workgroup(TEST_PARENT_WRKGRP)
        self.assertFalse(self.test_wrkgrp_name in parent.admin_workgroups)
