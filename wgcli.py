#!/usr/bin/env python
# CLI tool over Stanford Workgroup
# Demo script for workgroup.py
# Aug 12, 2015
# Written by S. Thiell <sthiell@stanford.edu>
# Contributors:
#   Marcello Golfieri <golfieri@stanford.edu>

import atexit
import os
import sys
import argparse
import logging

logging.basicConfig(
    stream=sys.stdout,
    level=os.environ.get("LOGLEVEL", "INFO"),
    format="%(levelname)s %(message)s",
)
logger = logging.getLogger(__name__)

if sys.version_info[0] < 3:
    raise Exception("Must be using Python 3")

from srcc.utils.workgroup import SUWorkgroupManager, SUWorkgroupAPIError

HISTFILE = os.path.join(os.path.expanduser("~"), ".wgcli_history")


def readline_setup():
    """Configure readline to automatically load and save a history file."""
    import readline

    readline.parse_and_bind("tab: complete")
    readline.set_completer_delims("")
    try:
        readline.read_history_file(HISTFILE)
    except IOError:
        pass
    atexit.register(readline.write_history_file, HISTFILE)


def print_workgroup(workgroupt, workgroup, maxdepth=1000, depth=1):
    print("%s%s: %s" % ("\t" * (depth - 1), workgroupt, workgroup.name))

    if depth > maxdepth:
        return

    print("%sMEMBER: %s" % ("\t" * depth, ", ".join(workgroup.members)))
    for app in workgroup.applications:
        print("%sAPP: %s" % ("\t" * depth, app))
    for admin in workgroup.applications:
        print("%sADMIN: %s" % ("\t" * depth, admin))
    for admin_app in workgroup.admin_apps:
        print("%sADMIN_APP: %s" % ("\t" * depth, admin_app))

    if depth > maxdepth:
        return

    for child in list(workgroup.workgroups.values()):
        print_workgroup("WORKGROUP", child, maxdepth, depth + 1)

    for child in list(workgroup.admin_workgroups.values()):
        print_workgroup("ADMIN_WORKGROUP", child, maxdepth, depth + 1)


class MainMenu(object):
    def __init__(self, base_url, cert, cert_key):
        # UAT/DEV url:
        #   https://aswsuat.stanford.edu/mais/workgroupsvc/v1/workgroups
        self.manager = SUWorkgroupManager(base_url, cert, cert_key)
        self.workgroup = None

    def usage(self, banner=False):
        if banner:
            print("""######################################
Welcome to Stanford Workgroup CLI tool
######################################
""")
        print("""Commands are:""")
        commands = [x for x in self.__dir__() if x.startswith('cmd_')] 
        # import IPython; IPython.embed()
        for c in commands:
            print(getattr(self, c).__doc__)

    def cmd_tree(self):
        "tree                              to list members recursively"
        print_workgroup("WORKGROUP", self.manager.workgroup(self.workgroup.name))

    def cmd_ls(self):
        """ls                                to list members"""
        print_workgroup(
            "WORKGROUP", self.manager.workgroup(self.workgroup.name), maxdepth=1
        )

    def cmd_workgroup(self, workgroup_name):
        """workgroup <workgroup>             to select a working workgroup"""
        logger.debug("loading workgroup {}...".format(workgroup_name))
        self.workgroup = self.manager.workgroup(workgroup_name, fetch_owners=False)
        logger.debug("workgroup loaded: {}".format(self.workgroup))

    def cmd_workgroup_full(self, workgroup_name):
        """workgroup_full <workgroup>        to select a working workgroup and load it with stem-owners"""
        logger.debug("loading workgroup in full {}...".format(workgroup_name))
        self.workgroup = self.manager.workgroup(workgroup_name, fetch_owners=True)
        logger.debug("workgroup fully loaded: {}".format(self.workgroup))

    def cmd_add(self, member_name):
        """add <sunet>                       to select a working workgroup"""
        self.workgroup.add_member(member_name)

    def cmd_remove(self, member_name):
        """remove <sunet>                    to select a working workgroup"""
        self.workgroup.remove_member(member_name)

    def cmd_reload(self):
        """reload                            to reload the workgroup fresh from server"""
        self.manager.clear()
        self.workgroup = self.manager.workgroup(self.workgroup.name)

    def cmd_exit(self):
        """exit                              to exit the CLI and return to shell"""
        sys.exit(0)

    def selector(self, argument):
        switcher = {
            "quit": ("cmd_exit", 0),
            "exit": ("cmd_exit", 0),
            "workgroup": ("cmd_workgroup", 1),
            "workgroup_full": ("cmd_workgroup_full", 1),
            "add": ("cmd_add", 1),
            "remove": ("cmd_remove", 1),
            "reload": ("cmd_reload", 0),
            "ls": ("cmd_ls", 0),
            "tree": ("cmd_tree", 0),
            "help": ("usage", 0),
        }
        logger.debug("switcher arg: {}".format(argument))
        selection = switcher.get(argument, "Invalid command")
        logger.debug("menu choice: {}".format(selection))
        return selection

    def main_menu(self):
        self.usage(banner=True)
        while True:
            prompt = "{}> ".format(self.workgroup.name) if self.workgroup else "#> "
            choice = input(prompt)
            dgrams = choice.split(" ")
            if len(dgrams) == 0:
                continue
            choice = dgrams[0]
            args = dgrams[1:]
            logger.debug("Got dgrams: {}".format(dgrams))
            logger.debug("choice: {}".format(choice))
            logger.debug("args: {}".format(args))
            try:
                selection = self.selector(choice)
                if not selection:
                    continue
                if choice in ["add", "remove", "reload", "ls", "tree"] and not self.workgroup:
                    print("Need to load a workgroup first")
                    self.usage()
                    continue
                cmd = selection[0]
                args_needed = selection[1]
                if args_needed != (len(args)):
                    print("Wrong number of args")
                    self.usage()
                    continue
                if args_needed > 0:
                    getattr(self, cmd)(*args)
                else:
                    getattr(self, cmd)()
            except SUWorkgroupAPIError as exc:
                print(exc)


if __name__ == "__main__":
    try:
        readline_setup()
    except ImportError:
        sys.stderr.write("Warning: failed to initialize readline")

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--wg_cert",
        action="store",
        dest="wg_cert",
        help="Workgroup Manager API Certificate",
        default="certs/xstream.stanford.edu.cert",
    )
    parser.add_argument(
        "--wg_cert_key",
        action="store",
        dest="wg_cert_key",
        help="Workgroup Manager API Key",
        default="certs/xstream.key",
    )
    parser.add_argument(
        "--base_url",
        action="store",
        dest="base_url",
        help="Workgroup Manager base API URL. e.g. specify  https://aswsuat.stanford.edu/mais/workgroupsvc/v1 to use UAT",
        default="https://workgroupsvc.stanford.edu/v1",
    )
    args = parser.parse_args()

    # wgcli script main entry point
    main = MainMenu(
        base_url=args.base_url, cert=args.wg_cert, cert_key=args.wg_cert_key
    )
    main.main_menu()
