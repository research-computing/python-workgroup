# About
This is the Python library for [Stanford Workgroup API](https://uit.stanford.edu/node/25176) that provides RESTful access to the Workgroup manager.
It has been developed by Stanford Research Computing for our HPC cluster accounting needs.

# Python Version
Python 2.6 or 2.7 are fully supported.
Python 3.x support still beta

# Third Party Libraries and Dependencies
The following library is required:
* [requests](http://docs.python-requests.org/)

# Author
Stephane Thiell

# Contributors
Marcello Golfieri

# Running
```
python wgcli.py
```
e.g. to run in UAT space:
```
./wgcli.py --wg_cert ~/work/secrets/workgroup/iedo_workgroup_api_dev.crt --wg_cert_key ~/work/secrets/workgroup/iedo_workgroup_api_dev.key --base_url=https://aswsuat.stanford.edu/mais/workgroupsvc/v1
```
# Testing
```
cd tests/
python  -m unittest workgroups_test
```
